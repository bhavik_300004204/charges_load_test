package com.myntra.loadtest;

import com.myntra.charge.ChargeClient;

public abstract class AbstractApplyCharge implements ApplyCharge {

    private ChargeClient chargeClient;

    public ChargeClient getChargeClient() {
        return chargeClient;
    }

    public AbstractApplyCharge(ChargeClient client) {
        this.chargeClient = client;
    }
}
