package com.myntra.loadtest;
import com.google.protobuf.DoubleValue;
import com.google.protobuf.StringValue;
import com.myntra.charge.ChargeClient;
import com.myntra.charge.dto.Charge;
import com.myntra.charge.dto.ChargeRule;
import com.myntra.charge.enums.ChargeLevel;
import com.myntra.charge.enums.ChargeType;
import com.myntra.charge.enums.RefundPolicy;
public class ActivateCoverFeeCharge extends AbstractApplyCharge{
	
    public ActivateCoverFeeCharge(ChargeClient client) {
        super(client);
    }
    
    @Override
    public void applyCharge() {
        ChargeClient client = getChargeClient();
        Charge request = Charge.newBuilder()
                .setName(StringValue.newBuilder().setValue("EORS_DEC_2019_COVERFEE").build())
                .setDescription(StringValue.newBuilder().setValue("CoverFeeMyntra_EORS").build())
                .setChargeType(ChargeType.coverfee)
                .setCreatedBy(StringValue.newBuilder().setValue("shubham.patel1@myntra.com").build())
                .setApplicableOn(ChargeLevel.CART)
                .setChargeOwner(com.myntra.charge.enums.ChargeOwner.SELLER)
                .setCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setChargeMasterMeta(StringValue.newBuilder().setValue("{\"sellerSplitEnabled\":true,\"sellerSplitOn\":\"AMOUNT\"}").build())
                .addChargeRules(ChargeRule.newBuilder()
                        .setChargeAmount(DoubleValue.newBuilder().setValue(199).build())
                        .setMinThreshold(DoubleValue.newBuilder().setValue(0).build())
                        .setMaxThreshold(DoubleValue.newBuilder().setValue(50000000).build())
                        .build())
                .build();
        client.createCharge(request, "2297");
    }
}