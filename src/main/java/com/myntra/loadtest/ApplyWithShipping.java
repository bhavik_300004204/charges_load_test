package com.myntra.loadtest;

import com.google.protobuf.DoubleValue;
import com.google.protobuf.Int32Value;
import com.google.protobuf.Int64Value;
import com.google.protobuf.StringValue;
import com.myntra.charge.ChargeClient;
import com.myntra.charge.dto.ChargeInputContext;
import com.myntra.charge.dto.ChargeOutputContext;
import com.myntra.charge.dto.ItemContext;
import com.myntra.charge.dto.ItemInputContext;
import com.myntra.charge.enums.ChargeType;
import com.myntra.charge.enums.ShippingMethod;
import com.myntra.loadtest.utils.SellerPartnerIdsUtil;

public class ApplyWithShipping extends AbstractApplyCharge {


    public ApplyWithShipping(ChargeClient client) {
        super(client);
    }

    /*TODO: In this method all the user level opted charges are false */
    @Override
    public void applyCharge() {

        DoubleValue doubleValue = DoubleValue.newBuilder().setValue(200.0).build();
        Int64Value skuid1 = Int64Value.newBuilder().setValue(69377).build();
        Int64Value skuid2 = Int64Value.newBuilder().setValue(69311).build();
        Int64Value sellerPartner1 = Int64Value.newBuilder().setValue(SellerPartnerIdsUtil.getRandomSellerPartnerId()).build();
        Int64Value sellerPartner2 = Int64Value.newBuilder().setValue(SellerPartnerIdsUtil.getRandomSellerPartnerId()).build();
        Int64Value seller1 = Int64Value.newBuilder().setValue(21).build();
        Int64Value seller2 = Int64Value.newBuilder().setValue(25).build();
        Int32Value qty1 = Int32Value.newBuilder().setValue(2).build();
        Int32Value qty2 = Int32Value.newBuilder().setValue(2).build();
        Int64Value articleTypeId = Int64Value.newBuilder().setValue(90).build();

        ItemContext itemContext1 = ItemContext.newBuilder().setDiscountedPrice(doubleValue).setMrp(doubleValue)
                .setArticleTypeId(articleTypeId).setStyleId(skuid1).setQuantity(qty1).setSkuId(skuid1)
                .setSellerId(seller1).setSellerPartnerId(sellerPartner1).build();
        ItemContext itemContext2 = ItemContext.newBuilder().setDiscountedPrice(doubleValue).setMrp(doubleValue)
                .setArticleTypeId(articleTypeId).setStyleId(skuid2).setQuantity(qty2).setSkuId(skuid1)
                .setSellerId(seller2).setSellerPartnerId(sellerPartner2).build();

        ItemInputContext itemInputContext1 = ItemInputContext.newBuilder().setItemContext(itemContext1).build();
        ItemInputContext itemInputContext2 = ItemInputContext.newBuilder().setItemContext(itemContext2).build();

        StringValue uidx = StringValue.newBuilder().setValue("user@myntra.com").build();

        /* TODO: applyCharge for CoverFee */
        ChargeInputContext chargeInputContext = ChargeInputContext.newBuilder()
                .setTotalValueWithoutBagDiscount(DoubleValue.of(800)).setTotalValueWithBagDiscount(DoubleValue.of(800))
                .setShippingMethod(ShippingMethod.NORMAL)
                .setUidx(uidx).addItemDetails(itemInputContext1).addItemDetails(itemInputContext2)
                .addItemDetails(itemInputContext2).addCartLevelCharges(ChargeType.giftwrap).setXMyntHeader(uidx).build();
        getChargeClient().applyCharge(chargeInputContext, "2297");
    }

}
