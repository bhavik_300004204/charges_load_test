package com.myntra.loadtest;

import com.myntra.charge.ChargeClient;
import com.myntra.loadtest.utils.ConnectionUtils;

import java.util.Random;
import java.util.stream.LongStream;

/**
 * Hello world!
 */
public class ChargeLoadTest {

    private void generateLoad(String host, Integer port) {
        final ChargeClient chargeClient = ConnectionUtils.getConnection(host, port);

        ApplyCharge[] applyChargeList = new ApplyCharge[11];

        applyChargeList[0] = new ApplyWithCodOpted(chargeClient);
        applyChargeList[1] = new ApplyWithCodTnb(chargeClient);
        applyChargeList[2] = new ApplyChargeWithCoverFeeOpted(chargeClient);
        applyChargeList[3] = new ApplyWithCodCover(chargeClient);
        applyChargeList[4] = new ApplyWithCoverTnb(chargeClient);
        applyChargeList[5] = new ApplyWithGiftWrapOpted(chargeClient);
        applyChargeList[6] = new ApplyWithShipping(chargeClient);
        applyChargeList[7] = new ApplyWithTNBOpted(chargeClient);
        applyChargeList[8] = new ApplyChargesAll(chargeClient);
        applyChargeList[9] = new ApplyCoverGiftWrap(chargeClient);
        applyChargeList[10] = new ApplywithCodGiftWrap(chargeClient);

        LongStream.rangeClosed(1, Long.MAX_VALUE).parallel().forEach(i -> {
            try {
                final long startTime = System.currentTimeMillis();
                applyChargeList[new Random().nextInt(894793274) % applyChargeList.length].applyCharge();
                System.out.println("Totam tike taken to proces requests # " + (System.currentTimeMillis() - startTime) + ", MS.");
            } catch (Throwable t) {
                //
            }
        });

    }
    
    private void productionSetup(String host, Integer port) {
    	
    	final ChargeClient chargeClient = ConnectionUtils.getConnection(host, port);

        ApplyCharge[] applyChargeList = new ApplyCharge[8];
        
        applyChargeList[0] = new CreateGiftWrapCharge(chargeClient);
        applyChargeList[1] = new CreateShippingCharge(chargeClient);
        applyChargeList[2] = new CreateTNBCharge(chargeClient);
        applyChargeList[3] = new DeactivateCODMyntra(chargeClient);
        applyChargeList[4] = new DeactivateCoverFeeMyntra(chargeClient);
        applyChargeList[5] = new UpdateGiftWrapMyntra(chargeClient);
        applyChargeList[6] = new UpdateShippingMyntra(chargeClient);
        applyChargeList[7] = new UpdateTNBMyntra(chargeClient);
        
        for(int i=0; i<8; i++) {
        	applyChargeList[i].applyCharge();
        }
    }

    private void normalShippingOn(String host, Integer port) {

        final ChargeClient chargeClient = ConnectionUtils.getConnection(host, port);

        ApplyCharge applyCharge = new UpdateShippingMyntra(chargeClient);
        applyCharge.applyCharge();
    }
    
    private void enableCoverfeeForEORS(String host, Integer port) {

        final ChargeClient chargeClient = ConnectionUtils.getConnection(host, port);

        ApplyCharge applyCharge = new ActivateCoverFeeCharge(chargeClient);
        applyCharge.applyCharge();
    }
    
    private void disableCoverFee(String host, Integer port) {

        final ChargeClient chargeClient = ConnectionUtils.getConnection(host, port);

        ApplyCharge applyCharge = new DeactivateCoverFeeMyntra(chargeClient);
        applyCharge.applyCharge();
    }

    public static void main(String[] args) {

        String host;
        try {
            host = args[0];
        } catch (Throwable t) {
            System.out.println("Host is not passed as considering null");
            host = null;
        }
        Integer port = null;
        try {
            String portStr = args[1];
            port = Integer.parseInt(portStr);
        } catch (Throwable t) {
            System.out.println("Port is not passed as considering null");
        }

        //new ChargeLoadTest().generateLoad(host, port);
        
       // new ChargeLoadTest().productionSetup(host, port);

       // new ChargeLoadTest().freeShippingOff(host, port);
        
        //new ChargeLoadTest().enableCoverfeeForEORS(host, port);
        
        new ChargeLoadTest().normalShippingOn(host, port);
        
        //new ChargeLoadTest().disableCoverFee(host, port);
        

    }
}
