package com.myntra.loadtest;

import com.google.protobuf.DoubleValue;
import com.google.protobuf.StringValue;
import com.myntra.charge.ChargeClient;
import com.myntra.charge.dto.Charge;
import com.myntra.charge.dto.ChargeRule;
import com.myntra.charge.enums.ChargeLevel;
import com.myntra.charge.enums.ChargeType;
import com.myntra.charge.enums.RefundPolicy;

public class CreateShippingCharge extends AbstractApplyCharge{

	public CreateShippingCharge(ChargeClient client) {
        super(client);
    }
	
	@Override
	public void applyCharge() {
	
		System.out.println( "Hello World!" );
		ChargeClient client = getChargeClient();
        Charge request = Charge.newBuilder()
                .setName(StringValue.newBuilder().setValue("ShippingChargeJabong").build())
                .setDescription(StringValue.newBuilder().setValue("ProductionTesting").build())
                .setChargeType(ChargeType.shipping)
                .setCreatedBy(StringValue.newBuilder().setValue("shubham.patel1@myntra.com").build())
                .setApplicableOn(ChargeLevel.CART)
                .setChargeOwner(com.myntra.charge.enums.ChargeOwner.SELLER)
                .setCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setChargeMasterMeta(StringValue.newBuilder().setValue("{\"sellerSplitEnabled\":false,\"categoryShippingFeeEnabled\":false,\"categoryShippingCharge\":49,\"categoryShippingChargeLimit\":499,\"isFreeShippingForFirstOrder\":true,\"shippingChargeLimitForFirstOrder\":499,\"returnAbuserShippingCharge\":149}").build())
                .addChargeRules(ChargeRule.newBuilder()
                        .setChargeAmount(DoubleValue.newBuilder().setValue(149).build())
                        .setMinThreshold(DoubleValue.newBuilder().setValue(0).build())
                        .setMaxThreshold(DoubleValue.newBuilder().setValue(998).build())
                        .build())
                .addChargeRules(ChargeRule.newBuilder()
                        .setChargeAmount(DoubleValue.newBuilder().setValue(0).build())
                        .setMinThreshold(DoubleValue.newBuilder().setValue(999).build())
                        .setMaxThreshold(DoubleValue.newBuilder().setValue(50000000).build())
                        .build())
                .build();

        /*TODO: Create Charge*/
        Long val = client.createCharge(request, "4603");
        System.out.println(val);
	}
}
