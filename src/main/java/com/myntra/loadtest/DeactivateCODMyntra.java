package com.myntra.loadtest;

import com.google.protobuf.Int64Value;
import com.myntra.charge.ChargeClient;

public class DeactivateCODMyntra extends AbstractApplyCharge{

	public DeactivateCODMyntra(ChargeClient client) {
        super(client);
    }
	
	@Override
	public void applyCharge() {
		
		ChargeClient client = getChargeClient();
		client.deActivateCharge(Int64Value.of(4), "2297");
	}
}
