package com.myntra.loadtest;

import com.google.protobuf.DoubleValue;
import com.google.protobuf.Int64Value;
import com.google.protobuf.StringValue;
import com.myntra.charge.ChargeClient;
import com.myntra.charge.dto.Charge;
import com.myntra.charge.dto.ChargeRule;
import com.myntra.charge.enums.ChargeLevel;
import com.myntra.charge.enums.ChargeType;
import com.myntra.charge.enums.RefundPolicy;

public class UpdateGiftWrapMyntra extends AbstractApplyCharge{

	public UpdateGiftWrapMyntra(ChargeClient client) {
        super(client);
    }
	
	@Override
	public void applyCharge() {
		ChargeClient client = getChargeClient();
		client.deActivateCharge(Int64Value.of(8), "2297");
		System.out.println("Successfully deactivated");
		
		Charge request = Charge.newBuilder()
                .setName(StringValue.newBuilder().setValue("GiftWrapChargeMyntra").build())
                .setDescription(StringValue.newBuilder().setValue("ProductionTesting").build())
                .setChargeType(ChargeType.giftwrap)
                .setCreatedBy(StringValue.newBuilder().setValue("shubham.patel1@myntra.com").build())
                .setApplicableOn(ChargeLevel.CART)
                .setChargeOwner(com.myntra.charge.enums.ChargeOwner.SELLER)
                .setCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedPacketCancellation(RefundPolicy.NO_REFUND)
                .setCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setNonCustomerAttributedReturns(RefundPolicy.NO_REFUND)
                .setChargeMasterMeta(StringValue.newBuilder().setValue("{\"sellerSplitEnabled\":false}").build())
                .addChargeRules(ChargeRule.newBuilder()
                        .setChargeAmount(DoubleValue.newBuilder().setValue(25).build())
                        .setMinThreshold(DoubleValue.newBuilder().setValue(0).build())
                        .setMaxThreshold(DoubleValue.newBuilder().setValue(50000000).build())
                        .build())
                .build();

        /*TODO: Create Charge*/
        Long val = client.createCharge(request, "2297");
        System.out.println(val);
	}
}
