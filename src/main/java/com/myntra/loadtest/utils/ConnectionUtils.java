package com.myntra.loadtest.utils;

import com.myntra.charge.ChargeClient;

public class ConnectionUtils {

    public static ChargeClient getConnection(String host, Integer port) {
        if (host == null || port == null)
            throw new RuntimeException("Host or null can not be null.");
        else
            return new ChargeClient(host, port, Constants.APPLY_CHARGE_TIMEOUT_MILLI_SECONDS);
    }

}
