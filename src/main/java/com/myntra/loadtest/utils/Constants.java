package com.myntra.loadtest.utils;

class Constants {
    static final Long APPLY_CHARGE_TIMEOUT_MILLI_SECONDS = 1000L;

    static final String LOG_FILE = "/tmp/charges_lt_client-response-time.log";

}
