package com.myntra.loadtest.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import java.io.*;
import java.util.*;

public class MessagePrinter {

    private final List<Double> requestTimes = new Vector<>();

    public MessagePrinter(long printFrequency) {


        Percentile percentileCalculator = new Percentile();

        new Timer().schedule(new TimerTask() {
            public void run() {
                PrintWriter outputFile = null;
                FileWriter fr = null;
                BufferedWriter br = null;
                try {
                    File file = new File(Constants.LOG_FILE);
                    fr = new FileWriter(file, true);
                    br = new BufferedWriter(fr);
                    outputFile = new PrintWriter(br);

                    final double[] timerValues;
                    synchronized (requestTimes) {
                        timerValues = ArrayUtils.toPrimitive(requestTimes.toArray(new Double[requestTimes.size()]));
                        requestTimes.clear();
                    }

                    final double _50PercentileValue = percentileCalculator.evaluate(timerValues, 50D);
                    final double _90PercentileValue = percentileCalculator.evaluate(timerValues, 90D);
                    final double _95PercentileValue = percentileCalculator.evaluate(timerValues, 95D);
                    final double _99PercentileValue = percentileCalculator.evaluate(timerValues, 99D);
                    final double _99_99PercentileValue = percentileCalculator.evaluate(timerValues, 99.99D);

                    outputFile.append(String.valueOf(new Date()))
                            .append(" --- Number of requests # ").append(String.valueOf(timerValues.length))
                            .append(" # 50 Percentile - ").append(String.valueOf(_50PercentileValue))
                            .append(" # 90 Percentile - ").append(String.valueOf(_90PercentileValue))
                            .append(" # 95 Percentile - ").append(String.valueOf(_95PercentileValue))
                            .append(" # 99 Percentile - ").append(String.valueOf(_99PercentileValue))
                            .append(" # 99.99 Percentile - ").append(String.valueOf(_99_99PercentileValue))
                            .append(System.lineSeparator());
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    if (outputFile != null) {
                        outputFile.close();
                        try {
                            br.close();
                        } catch (IOException e) {
//                            e.printStackTrace();
                        }
                        try {
                            fr.close();
                        } catch (IOException e) {
//                            e.printStackTrace();
                        }
                    }
                }
            }
        }, 0, printFrequency * 1000);
    }


    public void pushRequestTime(final Long requestTime) {
        requestTimes.add(requestTime.doubleValue());
    }


}
