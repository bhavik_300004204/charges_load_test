package com.myntra.loadtest.utils;

import java.util.Random;

public class PriceUtils {

    private static final double[] prices = new double[10000];

    static {
        for (int i = 0; i < prices.length; i++) {
            prices[i] = new Random().nextInt(313) * 31;
        }
    }

    public static double getRandomPrice() {
        return prices[new Random().nextInt(prices.length)];
    }

}
