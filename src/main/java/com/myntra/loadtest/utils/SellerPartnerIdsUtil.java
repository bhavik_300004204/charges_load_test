package com.myntra.loadtest.utils;

import java.util.Random;

public class SellerPartnerIdsUtil {

    private static final long[] sellerPartnerIds = new long[]{4076, 4024, 4027, 4036, 4028, 4118, 4214,4215,4216};

    public static long getRandomSellerPartnerId(){
        return sellerPartnerIds[new Random().nextInt(sellerPartnerIds.length)];
    }

}
